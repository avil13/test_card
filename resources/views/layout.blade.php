<!DOCTYPE html>
<html lang="en" ng-app="APP">

<head>
    <meta charset="UTF-8">
    <title>Тестовые кошельки</title>
    <link rel="stylesheet" href="/content/bwr/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/content/bwr/sweetalert/dist/sweetalert.css">
    <script src="/content/js/script.js"></script>
</head>

<body>
    <div class="container-fluid">
        <div class="row">
            <div class="view" ui-view></div>
            <div class="clearfix"></div>
        </div>
    </div>
</body>

</html>