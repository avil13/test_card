<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Group;
use App\User;
use App\Card;
use App\Transaction;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call(UserTableSeeder::class);

        $this->command->info('groupTable');
        $this->groupTable();

        $this->command->info('userTable');
        $this->userTable();

        $this->command->info('cardsTable');
        $this->cardsTable();

        $this->command->info('transactionsTable');
        $this->transactionsTable();

        Model::reguard();
    }


    private function groupTable()
    {
        Group::truncate();

        $data = [
                    [
                        'title' => 'Администраторы',
                        'system' => true,
                    ],
                    [
                        'title' => 'Пользователи',
                        'system' => false,
                    ],
                ];

        foreach ($data as $v) {
            Group::create($v);
        }
    }

    /**
     * заполнение пользователей
     */
    private function userTable()
    {
        User::truncate();

        $data = [
                    [
                        'name' => 'user1',
                        'password' => \Hash::make('user1'),
                        'email' => 'user1@mail.ru',
                        'group_id' => 2,
                    ],
                    [
                        'name' => 'user2',
                        'password' => \Hash::make('user2'),
                        'email' => 'user2@mail.ru',
                        'group_id' => 2,
                    ],
                    [
                        'name' => 'admin',
                        'password' => \Hash::make('admin'),
                        'email' => 'admin@mail.ru',
                        'group_id' => 1,
                    ],
                ];

        foreach ($data as $v) {
            User::create($v);
        }
    }

    /**
     * заполнение таблицы кошельков
     */
    private function cardsTable()
    {
        Card::truncate();

        $data = [
                    [
                        'user_id' => 1,
                        'title' => 'Simple',
                        'balance' => '',
                    ],
                    [
                        'user_id' => 2,
                        'title' => 'Главный',
                        'balance' => '',
                    ],
                    [
                        'user_id' => 2,
                        'title' => 'Для тусовок',
                        'balance' => '',
                    ],
                    [
                        'user_id' => 3,
                        'title' => 'Рабочий',
                        'balance' => '',
                    ],
                    [
                        'user_id' => 3,
                        'title' => 'Накопительный',
                        'balance' => '',
                    ],
                    [
                        'user_id' => 3,
                        'title' => 'Семейный',
                        'balance' => '',
                    ],
                ];

        foreach ($data as $v) {
            Card::create($v);
        }
    }


    /**
     * заполнение таблицы пранзакций
     */
    private function transactionsTable()
    {
        Transaction::truncate();

        $u = User::count();
        $c = Card::count();
        $i = 0;

        while ($i < 100) {
            ++$i;
            Transaction::create([
                'user_id' => rand(1, $u),
                'card_id' => rand(1, $c),
                'money' => rand(0, 10000),
                ]);
        }
    }
}
